package com.devcamp.customerinvoiceapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainInvoice {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> invoicesList() {
        Customer customer1 = new Customer(1, "Nguyen Van A", 10);
        Customer customer2 = new Customer(2, "Nguyen Van B", 20);
        Customer customer3 = new Customer(3, "Nguyen Van C", 30);

        Invoice invoice1 = new Invoice(1, 100.5, customer1);
        Invoice invoice2 = new Invoice(2, 199.9, customer2);
        Invoice invoice3 = new Invoice(3, 108.5, customer3);
        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);

        System.out.println(invoices);
        System.out.println("AmountAfterDiscount: " +
                invoice1.getAmountAfterDiscount());

        return invoices;

    }

}
